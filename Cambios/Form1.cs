﻿namespace Cambios
{
    using Servicos;
    using System;
    using System.Windows.Forms;
    using System.Collections.Generic;
    using Modelos;
    using System.Threading.Tasks;

    public partial class Form1 : Form
    {
        #region Atributos

        private List<Rate> Rates;

        private NetworkService networkService;

        private ApiService apiService;

        private DialogService dialogService;

        private DataService dataService;
        #endregion

        public Form1()
        {
            InitializeComponent();
            networkService = new NetworkService();
            apiService = new ApiService();
            dialogService=new DialogService();
            dataService=new DataService();
            LoadRates();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private async void LoadRates()
        {
            bool load;

            lbl_Resultado.Text = "A atualizar as taxas...";

            var connection = networkService.CheckConnection();

            if (!connection.IsSuccess)
            {
                LoadLocalRates();
                load = false;
            }
            else
            {
                await LoadApiRates();
                load = true;
            }

            if (Rates.Count == 0)
            {
                lbl_Resultado.Text = "Não ha ligação a internet e não foram previamente carregadas as taxas." + Environment.NewLine +
                                     "Tente mais Tarde!";
                return;
            }


            cb_MoedaOrigem.DataSource = Rates;
            cb_MoedaOrigem.DisplayMember = "Name";

            //Corrige bug microsoft
            cb_MoedaDestino.BindingContext = new BindingContext();

            cb_MoedaDestino.DataSource = Rates;
            cb_MoedaDestino.DisplayMember = "Name";


            lbl_Resultado.Text = "Taxas atualizadas!";

            if (load)
            {
                lbl_Status.Text = string.Format("Taxas carregadas da internet em {0:F}", DateTime.Now);
            }
            else
            {
                lbl_Status.Text = "Taxas carregadas da Base de Dados.";
            }

            progressBar1.Value = 100;

            btn_Converter.Enabled = true;
            btn_Troca.Enabled = true;
        }

        private void LoadLocalRates()
        {
             Rates = dataService.GetData();
        }

        private async Task LoadApiRates()
        {
            progressBar1.Value = 0;

            var response = await apiService.GetRates("http://apiexchangerates.azurewebsites.net", "/api/Rates");

            Rates = (List<Rate>)response.Result;

            dataService.SaveData(Rates);

        }

        private void btn_Converter_Click(object sender, EventArgs e)
        {
            Converter();
        }

        private void Converter()
        {
            if (string.IsNullOrEmpty(tb_Valor.Text))
            {
                dialogService.ShowMessage("Erro","Insira um valor a converter");
                return;
            }

            decimal valor;

            if (!decimal.TryParse(tb_Valor.Text, out valor))
            {
                dialogService.ShowMessage("Erro de conversão", "Valor tera que ser  numerico");
            }

            if (cb_MoedaOrigem.SelectedItem == null)
            {
                dialogService.ShowMessage("Erro", "Escolha uma moeda a converter!");
                return;
            }

            if (cb_MoedaDestino.SelectedItem == null)
            {
                dialogService.ShowMessage("Erro", "Escolha uma moeda de destino para converter!");
                return;
            }

            var taxaOrigem =(Rate) cb_MoedaOrigem.SelectedItem;
            var taxaDestino = (Rate)cb_MoedaDestino.SelectedItem;

            var valorConvertido = valor/(decimal) taxaOrigem.TaxRate*(decimal) taxaDestino.TaxRate;

            lbl_Resultado.Text = string.Format("{0} {1:C2} = {2} {3:C2}", taxaOrigem.Code, valor, taxaDestino.Code,
                valorConvertido);

        }

        private void btn_Troca_Click(object sender, EventArgs e)
        {
            Troca();
        }

        private void Troca()
        {
            var aux = cb_MoedaOrigem.SelectedItem;
            cb_MoedaOrigem.SelectedItem = cb_MoedaDestino.SelectedItem;
            cb_MoedaDestino.SelectedItem = aux;
            Converter();
        }
    }
}
