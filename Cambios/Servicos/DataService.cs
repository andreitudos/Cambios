﻿namespace Cambios.Servicos
{
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.IO;
    using Modelos;
    public class DataService
    {
        private SQLiteConnection connection;
        private SQLiteCommand command;
        private DialogService dialogService;


        public DataService()
        {
            dialogService = new DialogService();

            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }

            var path = @"Data\Rates.sqlite";

            try
            {
                connection = new SQLiteConnection("Data Source = " + path);
                connection.Open();
                string sqlcommand =
                    "create table if not exists rates(RateId, Code varchar(5), TaxRate real, Name varchar(250))";

                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                dialogService.ShowMessage("Erro SQL", ex.Message);
            }


        }

        public void SaveData(List<Rate> Rates)
        {
            try
            {
                foreach (var rate in Rates)
                {
                    string sql =
                        string.Format("insert into Rates (RateId, Code, TaxRate, Name) values({0},'{1}',{2},'{3}')",
                            rate.RateId, rate.Code, rate.TaxRate, rate.Name);

                    command = new SQLiteCommand(sql, connection);

                    command.ExecuteNonQuery();
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                dialogService.ShowMessage("Erro SQL", ex.Message);
            }
        }

        public List<Rate> GetData()
        {
            List<Rate> rates = new List<Rate>();

            try
            {
                string sql = "select * from Rates";

                command = new SQLiteCommand(sql, connection);

                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    rates.Add(new Rate
                    {
/*
                        RateId = reader.GetInt32(0),
                        Code = reader.GetString(1),
                        Name = reader.GetString(3),
                        TaxRate = reader.GetDouble(2)
                        */
                        
                                                RateId = (long) reader["RateId"],
                                                Code = (string) reader["Code"],
                                                Name = (string) reader["Name"],
                                                TaxRate = (double) reader["TaxRate"]
                                                
                    });
                }

                connection.Close();
                return rates;
            }
            catch (Exception ex)
            {
                dialogService.ShowMessage("Erro SQL000", ex.Message);
                return null;
            }
        }
    }
}
